Rails.application.routes.draw do
  resources :attractions, only: [:create, :destroy]

  root to: "pages#show"

  get "pages/show"
end
