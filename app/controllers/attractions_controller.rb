class AttractionsController < ApplicationController
  # Finds existing attraction by tripadvisor id of create a new one with data provided
  # Not a typical REST create action
  def create
    @attraction = Attraction.existing_of_new_by_id(attraction_id_from_params)

    if @attraction.update_attraction(attraction_params)
      respond_to do |format|
        format.json { render json: { success: true }, status: :created }
      end
    else
      respond_to do |format|
        format.json { render json: { success: false, errors: @attraction.errors }, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @attraction = Attraction.find_by(tripadvisor_attraction_id: params[:id])
    @attraction&.destroy

    respond_to do |format|
      format.js
    end
  end

  private

  def attraction_id_from_params
    params[:attraction].fetch(:tripadvisor_attraction_id, nil)
  end

  def find_attraction(id)
    Attraction.where(tripadvisor_attraction_id: id).first_or_initialize(tripadvisor_attraction_id: id)
  end

  def attraction_params
    params
      .require(:attraction)
      .permit(:tripadvisor_attraction_id, :score, :ratings, :locale, :name, :url, :rating_word)
  end
end
