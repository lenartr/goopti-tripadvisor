class Attraction < ActiveRecord::Base
  REFRESH_INTERVAL = 8.hours

  serialize :data, Hash

  validates :tripadvisor_attraction_id, presence: true, uniqueness: true

  def self.existing_of_new_by_id(tripadvisor_id)
    Attraction.where(tripadvisor_attraction_id: tripadvisor_id)
              .first_or_initialize(tripadvisor_attraction_id: tripadvisor_id)
  end

  # Find attraction by Tripadvisor id and given locale data. If locale data is
  # not available nil is returned.
  def self.find_attraction(id, locale:)
    attraction = Attraction.find_by(tripadvisor_attraction_id: id)
    attraction if attraction && attraction.data[locale.strip]
  end

  # Update attraction with data for specific locale
  # Shared data are saved as root attributes.
  # Localized data is extracted from params and saved in data hash.
  def update_attraction(params)
    locale = params.delete(:locale)

    data[locale] = {
      name: params.delete(:name)&.strip,
      url: params.delete(:url),
      rating_word: params.delete(:rating_word)&.strip
    }

    update(params.merge(updated_at: Time.current))
  end

  def cached_version_for(locale)
    cached_version_valid? && data[locale]
  end

  private

  def cached_version_valid?
    Time.current < (updated_at + REFRESH_INTERVAL)
  end
end
