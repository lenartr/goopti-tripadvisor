module AttractionsHelper
  def attraction_widget(id, locale:)
    attraction = Attraction.find_by(tripadvisor_attraction_id: id)

    if attraction && attraction.cached_version_for(locale)
      render partial: "tripadvisor/widget_static", locals: { attraction: attraction, locale: locale }
    else
      render partial: "tripadvisor/widget", locals: { location_id: id, locale: locale }
    end
  end
end
