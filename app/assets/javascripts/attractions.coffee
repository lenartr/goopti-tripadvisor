$ ->
  class App.Model.TripadvisorWidget
    constructor: (id, locale) ->
      @$el = $("[data-attraction-id='#{id}'][data-attraction-locale='#{locale}']")
      @attraction_id = id
      @locale = locale

      @poolForChanges()

    poolForChanges: (interval = 300)->
      @interval = setInterval(@updateAttraction, interval)

    updateAttraction: =>
      return false unless @$el.find(".cdsLocName a").length

      clearInterval(@interval)

      @name = @$el.find(".cdsLocName a").html()
      @url = @$el.find(".cdsLocName a").prop("href")
      @img = @$el.find(".cdsRating img").prop("src")
      @score = parseFloat(@img.substr(@img.length-7, 3))

      rating = @$el.find(".cdsRating span").html().match(/(\d+) (.+)/)
      @ratings = parseInt(rating[1])
      @rating_word = rating[2]

      @save()

    save: ->
      $.ajax(
        url: "/attractions",
        method: "POST",
        data:
          "attraction":
            "tripadvisor_attraction_id": @attraction_id,
            "score": @score,
            "ratings": @ratings,
            "locale": @locale,
            "name": @name,
            "url": @url,
            "rating_word": @rating_word
      )
