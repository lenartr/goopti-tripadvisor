require "rails_helper"

RSpec.describe Attraction, type: :model do
  it { is_expected.to validate_presence_of(:tripadvisor_attraction_id) }
  it { is_expected.to validate_uniqueness_of(:tripadvisor_attraction_id) }

  describe "#find_attraction" do
    context "with existing attraction" do
      let(:attraction) { FactoryGirl.create :attraction, :with_locales, tripadvisor_attraction_id: 123 }

      before do
        attraction
      end

      it "returns attraction if it exists and has locale data" do
        result = Attraction.find_attraction(123, locale: "en_US")

        expect(result).to eq attraction
      end

      it "returns nil if attraction exists but is missing locale data" do
        result = Attraction.find_attraction(123, locale: "es")

        expect(result).to be_nil
      end
    end

    context "with missing attraction" do
      it "returns nil" do
        result = Attraction.find_attraction(123, locale: "en_US")

        expect(result).to be_nil
      end
    end
  end

  describe ".update_attraction" do
    context "with new locale data" do
      let(:attraction) { FactoryGirl.create :attraction }
      let(:params) do
        {
          ratings: 10,
          score: 4.5,
          data: { de: { name: "German", url: "http://google.de" } }
        }
      end

      before do
        attraction.update_attraction(params)
      end

      it "creates locale in data" do
        expect(attraction.data).to have_key(:de)
      end

      it "creates locale data for name" do
        expect(attraction.data[:de][:name]).to eq "German"
      end

      it "creates locale data for url" do
        expect(attraction.data[:de][:url]).to eq "http://google.de"
      end

      it "creates rating" do
        expect(attraction.ratings).to eq 10
      end

      it "creates score" do
        expect(attraction.score).to eq 4.5
      end
    end

    context "with existing data" do
      let(:attraction) { FactoryGirl.create :attraction, :with_locales }
      let(:params) do
        {
          ratings: 20,
          score: 3.0,
          data: { en_US: { name: "English", url: "http://google.com" } }
        }
      end

      before do
        attraction.update_attraction(params)
      end

      it "updates locale in data" do
        expect(attraction.data).to have_key(:en_US)
      end

      it "updates locale data for name" do
        expect(attraction.data[:en_US][:name]).to eq "English"
      end

      it "updates locale data for url" do
        expect(attraction.data[:en_US][:url]).to eq "http://google.com"
      end

      it "creates rating" do
        expect(attraction.ratings).to eq 20
      end

      it "creates score" do
        expect(attraction.score).to eq 3.0
      end
    end

    it "touches updated_at (to bust cache)" do
      attraction = FactoryGirl.create :attraction

      expect { attraction.update_attraction({}) }.to change(attraction, :updated_at)
    end
  end

  describe ".cached_version_for" do
    xit "when cache is valid"
    xit "when cache is expired"
    xit "when data is missing"
  end
end
