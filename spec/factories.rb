FactoryGirl.define do
  factory :attraction do
    sequence(:tripadvisor_attraction_id) { |n| n }

    trait :with_locales do
      data do
        {
          "it" => {
            "name": "Venizia",
            "url": "http://tripadvisor.it/Venizia",
            "rating_word": "recensioni"
          },
          "en_US" => {
            "name": "Venice",
            "url": "http://tripadvisor.com/Venice",
            "rating_word": "reviews"
          }
        }
      end
    end
  end
end
