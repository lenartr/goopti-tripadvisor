class CreateAttractions < ActiveRecord::Migration
  def change
    create_table :attractions do |t|
      t.string :tripadvisor_attraction_id
      t.decimal :score
      t.integer :ratings
      t.text :data

      t.timestamps null: false
    end
  end
end
